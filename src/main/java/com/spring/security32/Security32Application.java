package com.spring.security32;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Security32Application {

	public static void main(String[] args) {
		SpringApplication.run(Security32Application.class, args);
	}

}
