package com.spring.security32.records;

public record ItemRecord(String itemName, Integer itemQty, String itemDesc, Integer itemPrice) {}
