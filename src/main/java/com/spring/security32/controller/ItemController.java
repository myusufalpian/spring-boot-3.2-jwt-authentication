package com.spring.security32.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spring.security32.dto.RequestNewItemDto;
import com.spring.security32.records.ItemRecord;
import com.spring.security32.service.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/items", produces = MediaType.APPLICATION_JSON_VALUE)
public class ItemController {
    private final ItemService itemService;

    @GetMapping("/")
    public ResponseEntity<?> getAllItem() throws JsonProcessingException {
        return itemService.getAllItem();
    }

    @PostMapping("/add")
    public ResponseEntity<?> addNewItem(@RequestHeader("userId") String userId, @RequestBody ItemRecord params) throws JsonProcessingException {
        System.out.println("userId controller: " + userId);
        return itemService.createNewItem(params);
    }
}
