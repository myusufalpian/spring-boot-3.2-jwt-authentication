package com.spring.security32.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spring.security32.dto.RequestSigninDto;
import com.spring.security32.dto.RequestSignupDto;
import com.spring.security32.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/signin")
    ResponseEntity<?> signin(@RequestBody RequestSigninDto param) throws JsonProcessingException {
        return authService.signin(param);
    }

    @PostMapping("/signup")
    ResponseEntity<?> signup(@RequestBody RequestSignupDto param) throws JsonProcessingException {
        return authService.signup(param);
    }
}
