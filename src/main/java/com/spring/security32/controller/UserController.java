package com.spring.security32.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spring.security32.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    @GetMapping("/")
    public ResponseEntity<?> getAllUser() throws JsonProcessingException {
        return userService.getAllUser();
    }

    
}
