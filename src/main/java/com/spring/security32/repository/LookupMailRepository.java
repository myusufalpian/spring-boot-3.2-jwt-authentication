package com.spring.security32.repository;

import com.spring.security32.model.LookupMail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface LookupMailRepository extends JpaRepository<LookupMail, Integer>{ 
    Optional<LookupMail> findByLookupMailCodeAndLookupMailStatus(String lookupMailCode, Integer lookupMailStatus);

    List<LookupMail> findByLookupMailStatus(Integer lookupMailStatus);
}
