package com.spring.security32.repository;

import com.spring.security32.model.UserOtp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserOtpRepository extends JpaRepository<UserOtp, Integer> {
    Optional<UserOtp> findByUserOtpCodeAndUserOtpStatus(String userOtpCode, Integer userOtpStatus);
}
