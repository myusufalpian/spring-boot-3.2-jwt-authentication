package com.spring.security32.repository;

import com.spring.security32.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Integer> {
    List<Item> findByItemStatus(Integer itemStatus);
}
