package com.spring.security32.repository;

import com.spring.security32.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {
    Optional<Users> findUserByUserEmailAndUserStatus(String userEmail, Integer userStatus);
    Optional<Users> findUserByUserUsernameAndUserStatus(String userUsername, Integer userStatus);
    @Query(nativeQuery = true, value = "SELECT a.* FROM public.usrs a where (a.usrs_usrnme = :username or a.usrs_email = :username) and a.usrs_sttus = :status")
    Optional<Users> findByUsernameOrEmailAndStatus(String username, Integer status);
}
