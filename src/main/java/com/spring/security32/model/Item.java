package com.spring.security32.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.spring.security32.records.ItemRecord;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;


@Entity
@Table(name = "itms")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Item {
    @Id
    @Column(name = "itms_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer itemId;
    @Column(name = "itms_uuid")
    String itemUuid;
    @Column(name = "itms_nme")
    String itemName;
    @Column(name = "itms_qty")
    Integer itemQty;
    @Column(name = "itms_dsc")
    String itemDesc;
    @Column(name = "itms_prce")
    Integer itemPrice;
    @Column(name = "itms_sttus")
    Integer itemStatus;
    @Column(name = "created_by")
    Integer createdBy;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
    @Column(name = "created_date")
    Date createdDate;
    @Column(name = "updated_by")
    Integer updatedBy;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
    @Column(name = "updated_date")
    Date updatedDate;

    public static Item fromRecord(ItemRecord itemRecord) {
        return new Item(1, UUID.randomUUID().toString(), itemRecord.itemName(), itemRecord.itemQty(), itemRecord.itemDesc(), itemRecord.itemPrice(), 0, 0, new Date(), null, null);
    }
}