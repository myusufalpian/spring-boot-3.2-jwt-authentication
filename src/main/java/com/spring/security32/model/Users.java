package com.spring.security32.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table(name = "usrs", schema = "public")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name = "usrs_id")
    private Integer userId;

    @Column(name = "usrs_uuid")
    private String userUuid;

    @Column(name = "usrs_email")
    private String userEmail;

    @Column(name = "usrs_usrnme")
    private String userUsername;

    @Column(name = "usrs_sttus")
    private Integer userStatus;

    @Column(name = "usrs_pass")
    private String userPass;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date createdDate;

    @Column(name = "updated_by")
    private Integer updatedBy;

    @Column(name = "updated_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date updatedDate;

}