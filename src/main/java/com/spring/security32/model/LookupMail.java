package com.spring.security32.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table(name = "lkup_mail", schema = "public")
public class LookupMail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name = "lkup_mail_id")
    private Integer lookupMailId;

    @Column(name = "lkup_mail_uuid")
    private String lookupMailUuid;

    @Column(name = "lkup_mail_code")
    private String lookupMailCode;

    @Column(name = "lkup_mail_sbject")
    private String lookupMailSubject;

    @Column(name = "lkup_mail_bdy")
    private String lookupMailBody;

    @Column(name = "lkup_mail_status")
    private Integer lookupMailStatus;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date createdDate;

    @Column(name = "updated_by")
    private Integer updatedBy;

    @Column(name = "updated_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date updatedDate;
}
