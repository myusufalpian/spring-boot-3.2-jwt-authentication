package com.spring.security32.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spring.security32.config.JwtUtil;
import com.spring.security32.dto.RequestSigninDto;
import com.spring.security32.dto.RequestSignupDto;
import com.spring.security32.model.LookupMail;
import com.spring.security32.model.Users;
import com.spring.security32.model.UserOtp;
import com.spring.security32.repository.LookupMailRepository;
import com.spring.security32.repository.UserOtpRepository;
import com.spring.security32.repository.UserRepository;
import com.spring.security32.utility.GenerateResponse;
import com.spring.security32.utility.Helper;
import com.spring.security32.utility.MailUtility;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final JwtUtil jwtUtil;
    private final ModelMapper mapper;
    private final LookupMailRepository lookupMailRepository;
    private final UserOtpRepository userOtpRepository;    
    private final MailUtility mailUtility;
    private final CustomUserDetailsService userDetailsService;

    public ResponseEntity<?> signin(RequestSigninDto param) throws JsonProcessingException {
        if (param.getUsername() == null || param.getUsername().isEmpty()) return GenerateResponse.badRequest("Username cannot be null", null);
        if (param.getPassword() == null || param.getPassword().isEmpty()) return GenerateResponse.badRequest("Password cannot be null", null);
        UserDetails userDetails = userDetailsService.loadUserByUsername(param.getUsername());
        if (userDetails == null) return GenerateResponse.notFound("User not found", null);
        if (!encoder.matches(param.getPassword(), userDetails.getPassword())) return GenerateResponse.badRequest("Password not matched", null);
        return GenerateResponse.success("Sign In Success", jwtUtil.generate(param.getUsername(), "ACCESS"));
    }

    @Transactional(rollbackOn = RuntimeException.class)
    public ResponseEntity<?> signup(RequestSignupDto param) throws JsonProcessingException {
        if (param.getUserEmail() == null || param.getUserEmail().isEmpty()) return GenerateResponse.badRequest("Email must be filled in", null);
        if (param.getUserUsername() == null || param.getUserUsername().isEmpty()) return GenerateResponse.badRequest("Username must be filled in", null);
        if (param.getUserPass() == null || param.getUserPass().isEmpty()) return GenerateResponse.badRequest("Password must be filled in", null);
        if (param.getUserConfirmPass() == null || param.getUserConfirmPass().isEmpty()) return GenerateResponse.badRequest("Confirmation Password must be filled in", null);
        if (!param.getUserPass().equals(param.getUserConfirmPass())) return GenerateResponse.badRequest("Password and confirmation password not match", null);
        Optional<Users> checkUsername = userRepository.findUserByUserUsernameAndUserStatus(param.getUserUsername(), 0);
        if (checkUsername.isPresent()) return GenerateResponse.badRequest("Username is already taken", null);
        Optional<Users> checkEmail = userRepository.findUserByUserEmailAndUserStatus(param.getUserEmail(), 0);
        if (checkEmail.isPresent()) return GenerateResponse.badRequest("Email is already taken", null);
        Users user = mapper.map(param, Users.class);
        user.setUserPass(encoder.encode(param.getUserPass()));
        user.setUserStatus(2);
        userRepository.saveAndFlush(user);

        UserOtp userOtp = new UserOtp();
        userOtp.setUserOtpCode(Helper.generateOtp());
        userOtp.setUserOtpExpiredDate(Helper.addTimes(new Date(), "minutes", 5));
        user.setCreatedBy(user.getUserId());
        userOtpRepository.save(userOtp);

        Optional<LookupMail> lookupMail = lookupMailRepository.findByLookupMailCodeAndLookupMailStatus("otp-activation-account", 0);
        if(lookupMail.isEmpty()) return GenerateResponse.notFound("email activation not found", null);
        mailUtility.sendEmail(user.getUserEmail(), lookupMail.get().getLookupMailSubject(), lookupMail.get().getLookupMailBody().replaceAll("$otp", userOtp.getUserOtpCode()));
        return GenerateResponse.created("Sign Up success, please check email for activation user", null);
    }

}
