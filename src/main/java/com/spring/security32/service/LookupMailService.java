package com.spring.security32.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spring.security32.dto.ResponseListLookupMail;
import com.spring.security32.model.LookupMail;
import com.spring.security32.repository.LookupMailRepository;
import com.spring.security32.utility.GenerateResponse;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class LookupMailService {
    private final LookupMailRepository lookupMailRepository;
    private final ModelMapper mapper;

    public ResponseEntity<?> listLookupMail() throws JsonProcessingException{
        List<LookupMail> list = lookupMailRepository.findByLookupMailStatus(0);
        return GenerateResponse.success("Get list lookup mail success", list.stream().map(x -> mapper.map(x, ResponseListLookupMail.class)).collect(Collectors.toList()));
    }
}
