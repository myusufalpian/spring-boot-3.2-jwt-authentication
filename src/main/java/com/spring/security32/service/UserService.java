package com.spring.security32.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spring.security32.model.Users;
import com.spring.security32.repository.UserRepository;
import com.spring.security32.utility.GenerateResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    public ResponseEntity<?> getAllUser()throws JsonProcessingException {
        List<Users> users = userRepository.findAll();
        return GenerateResponse.success("get All Data Success", users);
    }
}
