package com.spring.security32.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spring.security32.dto.RequestNewItemDto;
import com.spring.security32.dto.ResponseListItemDto;
import com.spring.security32.model.Item;
import com.spring.security32.records.ItemRecord;
import com.spring.security32.repository.ItemRepository;
import com.spring.security32.utility.GenerateResponse;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class ItemService {
    private final ItemRepository itemRepository;
    private final ModelMapper mapper;
    public ResponseEntity<?> getAllItem() throws JsonProcessingException {
        try {
            List<Item> items = itemRepository.findByItemStatus(0);
            return GenerateResponse.success("Get All Data Item Success", items.stream().map(x -> mapper.map(x, ResponseListItemDto.class)).toList());
        } catch (Exception e) {
            e.getStackTrace();
            return GenerateResponse.error(e.getLocalizedMessage(), null);
        }
    }

    @Transactional
    public ResponseEntity<?> createNewItem(ItemRecord param) throws JsonProcessingException {
        try {
            itemRepository.save(Item.fromRecord(param));
            return GenerateResponse.created("New Item successfully created", null);
        } catch (Exception e) {
            e.getStackTrace();
            return GenerateResponse.error(e.getLocalizedMessage(), null);
        }
    }
}
