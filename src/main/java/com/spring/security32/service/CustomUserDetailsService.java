package com.spring.security32.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.spring.security32.model.Users;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.spring.security32.repository.UserRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("username: " + username);
        Optional<Users> users = userRepository.findByUsernameOrEmailAndStatus(username, 0);
        if (users.isEmpty()) return null;
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        return User.builder().username(users.get().getUserUsername()).password(users.get().getUserPass()).roles(roles.toArray(new String[0])).build();
    }

    public String userId(String username) {
        Optional<Users> user = userRepository.findUserByUserUsernameAndUserStatus(username, 0);
        return user.map(value -> value.getUserId().toString()).orElse(null);
    }
    
}
