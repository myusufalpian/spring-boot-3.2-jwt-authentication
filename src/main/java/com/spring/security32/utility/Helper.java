package com.spring.security32.utility;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Helper {
    public static String generateOtp() {
        Random random = new Random();
        int randomNumber = random.nextInt(900000) + 100000;
        return String.valueOf(randomNumber);
    }

    public static Date addTimes(Date date, String type, Integer times) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (type.equalsIgnoreCase("minutes")) {
            calendar.add(Calendar.MINUTE, times);
        } else if (type.equalsIgnoreCase("hours")) {
            calendar.add(Calendar.HOUR, times);
        } else if (type.equalsIgnoreCase("days")) {
            calendar.add(Calendar.DAY_OF_MONTH, times);
        }
        return calendar.getTime();
    }

}
