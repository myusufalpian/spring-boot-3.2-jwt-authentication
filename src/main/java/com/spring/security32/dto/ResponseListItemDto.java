package com.spring.security32.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Column;
import lombok.Data;

import java.util.Date;

@Data
public class ResponseListItemDto {
    String itemUuid;
    String itemName;
    Integer itemQty;
    String itemDesc;
    Integer itemPrice;
    Integer createdBy;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
    Date createdDate;
    Integer updatedBy;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
    Date updatedDate;
}
