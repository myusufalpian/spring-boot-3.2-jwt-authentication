package com.spring.security32.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ResponseListLookupMail {
    private String lookupMailUuid;
    private String lookupMailCode;
    private String lookupMailSubject;
    private String lookupMailBody;
    private Integer createdBy;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date createdDate;
    private Integer updatedBy;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date updatedDate;
}
