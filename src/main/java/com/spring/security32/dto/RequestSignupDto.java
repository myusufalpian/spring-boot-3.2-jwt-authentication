package com.spring.security32.dto;

import lombok.Data;

@Data
public class RequestSignupDto {
    private String userUsername;
    private String userEmail;
    private String userPass;
    private String userConfirmPass;
    private String userFirstName;
    private String userLastName;
    private String userPhone;
}
