package com.spring.security32.dto;

public record RequestNewItemDto(
        String itemName,
        Integer itemQty,
        String itemDesc
) {
}
