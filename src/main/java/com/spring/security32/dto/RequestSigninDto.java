package com.spring.security32.dto;

import lombok.Data;

@Data
public class RequestSigninDto {
    private String username;
    private String password;
}
